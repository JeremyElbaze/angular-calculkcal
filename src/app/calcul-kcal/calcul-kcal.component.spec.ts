import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CalculKcalComponent } from './calcul-kcal.component';

describe('CalculKcalComponent', () => {
  let component: CalculKcalComponent;
  let fixture: ComponentFixture<CalculKcalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CalculKcalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CalculKcalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
