import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-calcul-kcal',
  templateUrl: './calcul-kcal.component.html',
  styleUrls: ['./calcul-kcal.component.scss']
})

export class CalculKcalComponent implements OnInit {
  private sexe: any;
  private weightGoal: any;
  private age: any;
  private size: any;
  private weight: any;
  private activity: any;

  constructor() {}

  ngOnInit(): void {
  }

  public onSubmit(form: NgForm){
    this.sexe = form.value.sexe;
    this.weightGoal = form.value.weightGoal;
    this.age = parseInt(form.value.age);
    this.size = parseInt(form.value.size);
    this.weight = parseInt(form.value.weight);
    this.activity = parseFloat(form.value.activity);

    this.calcul();
  }

  public calcul(): void{
    if(this.sexe == "men"){
      const kcal: number = ((66.5 + (13.75 * this.weight) + (5 * this.size)) - ((6.77 * this.age))) * this.activity;
      //return formule;
    }else{
      const kcal: number = ((655.1 + (9.56 * this.weight) + (1.85 * this.size)) - ((4.67 * this.age))) * this.activity;
      //return 2;
    }
    //console.log(this.sexe, this.weightGoal, this.age, this.size, this.weight, this.activity);
  }


  // -- Getters --
  public get getSexe(){
    return this.sexe;
  }

  public get getWeightGoal(){
    return this.weightGoal;
  }

  public get getAge(){
    return this.age;
  }

  public get getSize(){
    return this.size;
  }

  public get getWeight(){
    return this.weight;
  }

  public get getActivity(){
    return this.activity;
  }

}